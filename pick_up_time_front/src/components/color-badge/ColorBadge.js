import PropTypes from 'prop-types';
// @mui
import { alpha } from '@mui/material/styles';
import { Box, Stack } from '@mui/material';

// ----------------------------------------------------------------------

ColorBadge.propTypes = {
  sx: PropTypes.object,
  colors: PropTypes.arrayOf(PropTypes.string),
};

export default function ColorBadge({ color, sx }) {
  return (
    <Stack component="span" direction="row" justifyContent={"center"} sx={sx}>
        <Box
          sx={{
            ml: -0.75,
            width: 20,
            height: 20,
            borderRadius: '50%',
            border: (theme) => `solid 2px ${theme.palette.background.paper}`,
            boxShadow: (theme) => `inset -1px 1px 2px ${alpha(theme.palette.common.black, 0.24)}`,
            bgcolor: color,
          }}
        />
    </Stack>
  );
}
