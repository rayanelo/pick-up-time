import React from 'react'
import { useLottie } from "lottie-react";
import WorkLottie from '../assets/lotties/working.json'
import HelpLottie from '../assets/lotties/help.json'
import ManagmentLottie from '../assets/lotties/management.json'
import RelaxingLottie from '../assets/lotties/relaxing.json'

const LOTTIESTAB = {
    1: {
        loop: true,
        autoplay: true,
        animationData: WorkLottie,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        },
        height : 600,
        width : 600,
    },
    2: {
        loop: true,
        autoplay: true,
        animationData: HelpLottie,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        },
        height : 500,
        width : 500,
    },
    3: {
        loop: true,
        autoplay: true,
        animationData: ManagmentLottie,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        },
        height : 500,
        width : 500,
    },
    4: {
        loop: true,
        autoplay: true,
        animationData: RelaxingLottie,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        },
        height : 500,
        width : 800,
    },
};

export default function Lottie({ option }) {
    const { View } = useLottie(LOTTIESTAB[option], { height: LOTTIESTAB[option].height, width: LOTTIESTAB[option].width });

    return (
        <React.Fragment >
            {View}
        </React.Fragment >
    )
}