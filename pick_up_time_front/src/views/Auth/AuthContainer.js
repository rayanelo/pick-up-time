import React, { useState } from 'react';
import { useNavigate, Link as RouterLink, useLocation } from "react-router-dom";

// form
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// redux
import { connect } from 'react-redux';
import * as actions from '../../redux/actions'
//mui
import { Link, Stack, Alert, IconButton, InputAdornment } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// components
import Iconify from '../../components/iconify/Iconify';
import FormProvider from '../../components/hook-form/FormProvider';
import RHFTextField from '../../components/hook-form/RHFTextField';

function AuthContainer(props) {
    const navigate = useNavigate();
    let location = useLocation();
    const [showPassword, setShowPassword] = useState(false);

    const LoginSchema = Yup.object().shape({
        username: Yup.string().required('Username is required'),
        password: Yup.string().required('Password is required'),
    });

    //waring --- ici ---- cause : value = null
    const defaultValues = {
        username: null,
        password: null,
    };
    const methods = useForm({
        resolver: yupResolver(LoginSchema),
        defaultValues,
    });

    const {
        reset,
        setError,
        handleSubmit,
        formState: { errors, isSubmitting, isSubmitSuccessful },
    } = methods;


    React.useEffect(() => {
        let from = location.state?.from?.pathname || "/";
        if (props.error === true) {
            reset();
            setError('afterSubmit', {
                type: 'manual',
                message: "Le nom d'utilisateur ou le mot de passe est incorrect"
            });
        }
        if (props.isAuthenticated) {
            navigate(from, { replace: true });
        }
    }, [location.state?.from?.pathname, navigate, reset, setError, props.error, props.isAuthenticated]);

    const onSubmit = (inputform) => {
        props.onAuth(inputform.username, inputform.password)
    };

    return (
        <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
            <Stack spacing={3}>
                {!!errors.afterSubmit && <Alert severity="error">{errors.afterSubmit.message}</Alert>}

                <RHFTextField name="username" label="Nom de comptre" />

                <RHFTextField
                    name="password"
                    label="Mot de passe"
                    type={showPassword ? 'text' : 'password'}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                                    <Iconify icon={showPassword ? 'eva:eye-fill' : 'eva:eye-off-fill'} />
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
            </Stack>

            <Stack alignItems="flex-end" sx={{ my: 2 }}>
                <Link
                    component={RouterLink}
                    to={"#"}
                    variant="body2"
                    color="inherit"
                    underline="always"
                >
                    Mot de passe oublié ?
                </Link>
            </Stack>
            <LoadingButton
                fullWidth
                color="inherit"
                size="large"
                type="submit"
                variant="contained"
                loading={isSubmitSuccessful || isSubmitting}
                sx={{
                    bgcolor: 'text.primary',
                    color: (theme) => (theme.palette.mode === 'light' ? 'common.white' : 'grey.800'),
                    '&:hover': {
                        bgcolor: 'text.primary',
                        color: (theme) => (theme.palette.mode === 'light' ? 'common.white' : 'grey.800'),
                    },
                }}
            >
                Connection
            </LoadingButton>
        </FormProvider>
    );
}


const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(actions.auth(username, password))
    };
};

const mapStateToProps = state => {
    return {
        isAuthenticated: state.isAuthenticated,
        error: state.error,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthContainer);