import { Link as RouterLink } from 'react-router-dom';
// @mui
import { Stack, Typography, Link } from '@mui/material';
// layouts
import LoginLayout from '../../layouts/login';
//
import AuthContainer from './AuthContainer';
// ----------------------------------------------------------------------

export default function Login() {

  return (
    <LoginLayout>
      <Stack spacing={2} sx={{ mb: 5, position: 'relative' }}>
        <Typography variant="h4">Connecter vous à votre compte</Typography>

        <Stack direction="row" spacing={0.5}>
          <Link component={RouterLink} to={"#"} variant="subtitle2">
            Nous contacter pour crée un compte
          </Link>
        </Stack>
      </Stack>
      <AuthContainer />

    </LoginLayout>
  );
}
