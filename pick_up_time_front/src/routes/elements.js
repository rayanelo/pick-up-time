import { Suspense, lazy } from 'react';
// components
import LoadingScreen from '../components/loading-screen';

// ----------------------------------------------------------------------

const Loadable = (Component) => (props) =>
  (
    <Suspense fallback={<LoadingScreen />}>
      <Component {...props} />
    </Suspense>
  );

// ----------------------------------------------------------------------

export const UserManagement = Loadable(lazy(() => import('../pages/UserManagement/UserManagementContainer')));
export const SheduleManagement = Loadable(lazy(() => import('../pages/SheduleManagement/SheduleManagement')));
export const TimeClock = Loadable(lazy(() => import('../pages/TimeClock/TimeClock')));
export const Page404 = Loadable(lazy(() => import('../pages/Page404')));
