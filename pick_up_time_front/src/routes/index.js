import React from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
// layouts
import CompactLayout from '../layouts/compact';
import DashboardLayout from '../layouts/navbar';
// config
import { PATH_AFTER_LOGIN } from '../config-global';
//
import { connect } from 'react-redux';
import * as actions from '../redux/actions'
//
import {
  Page404,
  UserManagement,
  SheduleManagement,
  TimeClock,
} from './elements';

import Login from "../views/Auth/Login"
import GuestGuard from "../views/Auth/GuestGuard"
import AuthGuard from "../views/Auth/AuthGuard"
// ----------------------------------------------------------------------

const Router = (props) => {

  console.log(props)
  React.useEffect(() => {
    props.onTryAutoSignup()
  }, [props]);

  return useRoutes([
    {
      path: '/',
      children: [
        { element: <Navigate to={PATH_AFTER_LOGIN} replace />, index: true },
        {
          path: 'login',
          element: (
            <GuestGuard isAuthenticated={props.isAuthenticated} loading={props.loading}>
              <Login />
            </GuestGuard>
          ),
        },
      ],
    },
    {
      path: '/',
      element: (
        <AuthGuard isAuthenticated={props.isAuthenticated} loading={props.loading}>
          <DashboardLayout />
        </AuthGuard>
      ),
      children: [
        { element: <Navigate to={PATH_AFTER_LOGIN} replace />, index: true },
        {
          path: 'user_management',
          element: props.role === 'admin' ? <UserManagement /> : <Navigate to="/404" replace />
        },
        {
          path: 'shedule_management',
          element: <SheduleManagement />
        },
        {
          path: 'time_clock',
          element: <TimeClock />
        },
      ],
    },
    {
      element: <CompactLayout />,
      children: [{ path: '404', element: <Page404 /> }],
    },
    { path: '*', element: <Navigate to="/404" replace /> },
  ]);
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.isAuthenticated,
    loading: state.loading,
    error: state.error,
    role: state.role,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Router);
