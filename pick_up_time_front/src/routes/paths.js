// ----------------------------------------------------------------------

function path(root, sublink) {
  return `${root}${sublink}`;
}

const ROOTS_DASHBOARD = '';

// ----------------------------------------------------------------------

export const PATH_DASHBOARD = {
  root: ROOTS_DASHBOARD,
  user_management: path(ROOTS_DASHBOARD, '/user_management'),
  shedule_management: path(ROOTS_DASHBOARD, '/shedule_management'),
  time_clock: path(ROOTS_DASHBOARD, '/time_clock'),
};
