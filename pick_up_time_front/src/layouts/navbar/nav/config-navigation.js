// components'
import SvgColor from '../../../components/svg-color';

// ----------------------------------------------------------------------

const icon = (name) => (
  <SvgColor src={`/assets/icons/navbar/${name}.svg`} sx={{ width: 1, height: 1 }} />
);

const ICONS = {
  user: icon('ic_user'),
  calendar: icon('ic_calendar'),
  timer: icon('ic_timer'),
};

const navConfig = [
  // GENERAL
  // ----------------------------------------------------------------------
  {
    subheader: 'general',
    items: [
      { title: "Relevé d'heures", path: "/shedule_management", icon: ICONS.calendar },
      { title: 'Pointeuse', path: "/time_clock", icon: ICONS.timer },
    ],
  },

];

export default navConfig;
