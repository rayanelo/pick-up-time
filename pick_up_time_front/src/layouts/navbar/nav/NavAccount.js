import { Link as RouterLink } from 'react-router-dom';
// @mui
import { styled, alpha } from '@mui/material/styles';
import { Box, Link, Typography } from '@mui/material';
// auth
// routes
// import { PATH_DASHBOARD } from '../../../routes/paths';
// components
import { CustomAvatar } from '../../../components/custom-avatar';
import { connect } from 'react-redux';


// ----------------------------------------------------------------------

const StyledRoot = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(2, 2.5),
  borderRadius: Number(theme.shape.borderRadius) * 1.5,
  backgroundColor: alpha(theme.palette.grey[500], 0.12),
}));

// ----------------------------------------------------------------------

export function NavAccount(props) {
  const user = {
    displayName: 'John Doe',
    role: 'johndoe@example.com',
    address: '123 Main St'
  };
  return (
    <Link component={RouterLink} to={"/"} underline="none" color="inherit">
      <StyledRoot>
        <CustomAvatar src={user?.photoURL} alt={props?.lastname + props?.firstname} name={props?.lastname + props?.firstname} />

        <Box sx={{ ml: 2, minWidth: 0 }}>
          <Typography variant="subtitle2" noWrap>
            {props?.username}
          </Typography>

          <Typography variant="body2" noWrap sx={{ color: 'text.secondary' }}>
            {props?.role}
          </Typography>
        </Box>
      </StyledRoot>
    </Link>
  );
}

const mapStateToProps = state => {
  return {
    firstname: state.firstname,
    lastname: state.lastname,
    role: state.role,
    username: state.username,
  };
};

export default connect(mapStateToProps)(NavAccount);
