import React from 'react';
import Router from './routes';

import { HelmetProvider } from 'react-helmet-async';
//Redux
import { Provider } from 'react-redux';
import { store } from "./redux/store";
//theme
import ThemeProvider from './theme';
import SnackbarProvider from './components/snackbar/SnackbarProvider';
// setting
import { SettingsProvider } from './components/settings';
import { ThemeSettings } from './components/settings';
//


export default function App() {
  return (
    <HelmetProvider>
      <Provider store={store}>
        <SettingsProvider>
          <ThemeProvider>
            <ThemeSettings>
              <SnackbarProvider>
                <Router/>
              </SnackbarProvider>
            </ThemeSettings>
          </ThemeProvider>
        </SettingsProvider>
      </Provider>
    </HelmetProvider>
  )
}