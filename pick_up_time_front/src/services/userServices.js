import axios from 'axios'
import { CREATE_USER, DELETE_USER, GET_ALL_USERS } from "./CONSTANTS";

export const createUser = (data) => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.post(CREATE_USER(), data)
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("Create User > axios err=", err);
                    reject("Error in send Create User axios!");
                });
        } catch (error) {
            console.error("in UserServices > Create User, Err===", error);
        }
    });
}

export const getAllUser = () => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.get(GET_ALL_USERS())
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("get all users > axios err=", err);
                    reject("Error in get all users  axios!");
                });
        } catch (error) {
            console.error("in UserServices > Create User, Err===", error);
        }
    });
};

export const deleteUser = (id) => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.delete(DELETE_USER(id))
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("deleteUser > axios err=", err);
                    reject("Error in deleteUser axios!");
                });
        } catch (error) {
            console.error("in deleteUser > user, Err===", error);
        }
    });
}
