/**
 * All API urls and other constants will reside here.
 * It is always a good idea to keep a local copy of all API response to
 * keep your app working for UI changes and
 * make it independent of network requirements.
 *
 * They need to be categorised and grouped together as:
 *  - Actual endpoints url.
 *  - Local data .json file path.
 * At a moment only one group should be uncommented.
 *
 * Other way to deal with this is to name every json file as per your service endpoint and use a basepath variable.
 * Toggle this basePath variable between "actual-domain.com/" or "/data/".
 */

// Local endpoints. Uncomment below section to use dummy local data.

//auth
export const AUTHENTICATION = () => `/login`;
export const LOGOUT = () => `/logout`;

// user
export const CREATE_USER = () => `/createUser`;
export const GET_ALL_USERS = () => `/getAllUsers`;
export const DELETE_USER = (id) => `/deleteUser?id=${id}`;

// pick up time
export const CREATE_PICK_UP_TIME = () => `/createPickUpTime`;
export const GET_ALLPICK_UP_TIME = () => `/getAllPickUpTime`;
export const GET_LAST_PICK_UP_TIME_BY_USER = () => `/getLastPickUpTimeByUser`;
export const GET_PICK_UP_TIME_BY_USER = () => `/getPickUpTimeByUser`;
export const PUT_PICK_UP_TIME = () => '/putPickUpTime';