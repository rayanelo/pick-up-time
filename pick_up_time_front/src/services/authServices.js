// All user related database operations can be defined here.
import axios from 'axios'
import { AUTHENTICATION, LOGOUT } from "./CONSTANTS";

/**
 * Function to authentication the user.
 * @param {string} email of the user.
 * @param {string} password of the user.
**/
export const authentication = (credentials) => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.post(AUTHENTICATION(), credentials)
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("authentication > axios err=", err);
                    reject("Error in authentication axios!");
                });
        } catch (error) {
            console.error("in userServices > authentication, Err===", error);
        }
    });
};

/**
 * Function to auto authentication the user with cookie.
**/
export const autoAuthentication = () => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.get(AUTHENTICATION())
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    reject("Error in autoAuthentication !");
                });
        } catch (error) {
            console.error("in userServices > autoAuthentication, Err===", error);
        }
    });
};

/**
 * Function to logout the user.
**/
export const logout = () => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.get(LOGOUT())
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    reject("Error in logout !");
                });
        } catch (error) {
            console.error("in userServices > logout, Err===", error);
        }
    });
};