import axios from 'axios'
import {
    CREATE_PICK_UP_TIME,
    GET_ALLPICK_UP_TIME,
    GET_LAST_PICK_UP_TIME_BY_USER,
    PUT_PICK_UP_TIME,
    GET_PICK_UP_TIME_BY_USER
} from "./CONSTANTS";

export const createPickUpTime = (data) => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.post(CREATE_PICK_UP_TIME(), data)
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("Create Pick Up Time > axios err=", err);
                    reject("Error in send Create Pick Up Time axios!");
                });
        } catch (error) {
            console.error("in Create Pick Up Time > Create Pick Up Time, Err===", error);
        }
    });
}

export const getAllPickUpTime = () => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.get(GET_ALLPICK_UP_TIME())
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("get all pick up time  > axios err=", err);
                    reject("Error in get all pick up time  axios!");
                });
        } catch (error) {
            console.error("in Pick up time > get all pick up User, Err===", error);
        }
    });
};

export const getLastPickUpTimeByUser = () => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.get(GET_LAST_PICK_UP_TIME_BY_USER())
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("get last pick up time  > axios err=", err);
                    reject("Error in get last pick up time  axios!");
                });
        } catch (error) {
            console.error("in Pick up time > get last pick up User, Err===", error);
        }
    });
};

export const getPickUpTimeByUser = () => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.get(GET_PICK_UP_TIME_BY_USER())
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("get last pick up time  > axios err=", err);
                    reject("Error in get last pick up time  axios!");
                });
        } catch (error) {
            console.error("in Pick up time > get last pick up User, Err===", error);
        }
    });
};

export const putPickUpTime = (data) => {
    return new Promise((resolve, reject) => {
        try {
            axios.defaults.withCredentials = true
            axios.put(PUT_PICK_UP_TIME(), data)
                .then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    console.log("put pick up time  > axios err=", err);
                    reject("Error in put pick up time  axios!");
                });
        } catch (error) {
            console.error("in Pick up time > put pick up User, Err===", error);
        }
    });
}