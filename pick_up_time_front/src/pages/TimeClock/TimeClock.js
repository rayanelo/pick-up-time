import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// @mui
import {
    Stack,
    Typography,
    DialogTitle,
    DialogContent,
    Dialog,
    Button,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { createPickUpTime, getLastPickUpTimeByUser, putPickUpTime } from '../../services';
import { useSnackbar } from '../../components/snackbar';

const CircleButton = styled(Button)({
    borderRadius: '50%',
    width: 100,
    height: 100,
    minWidth: 0,
    padding: 0,
});

// ----------------------------------------------------------------------
TimeClock.propTypes = {
    date: PropTypes.instanceOf(Date)
};

export function TimeClock(props) {
    const { date } = props;
    const { enqueueSnackbar } = useSnackbar();
    const [currentTime, setCurrentTime] = React.useState(new Date());
    const [pickUpTime, setPickUpTime] = React.useState()
    const [open, setOpen] = React.useState(false);
    const [checkStatus, setCheckStatus] = React.useState(false)

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    React.useEffect(() => {
        const timerId = setInterval(() => {
            setCurrentTime(new Date());
        
        }, 10000);
        return () => clearInterval(timerId);
    }, []);

    React.useEffect(() => {
        Promise.all([
            getLastPickUpTimeByUser(),
        ]).then(([
            responseLastPickUpTimeByUser,
        ]) => {
            setPickUpTime(responseLastPickUpTimeByUser.pick_up_time)
        }).catch(err => {
            console.log(err);
        });
    }, [checkStatus])

    const formattedDate = Intl.DateTimeFormat('fr-FR', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    }).format(date);

    const formattedTime = new Intl.DateTimeFormat('fr-FR', {
        hour: 'numeric',
        minute: 'numeric',
    }).format(currentTime);

    const handleCreatePickUpTime = () => {
        let data = {}
        data.user_reference = props.id
        console.log(props.username)
        data.username = props.username
        data.status = 'arrived validated'
        createPickUpTime(data).then((r) => {
            console.log(r)
            setCheckStatus(!checkStatus)
            enqueueSnackbar('succes', {
                variant: 'success',
            });
        }).catch(err => {
            console.log(err)
            enqueueSnackbar("error", {
                variant: 'error',
            });
        });
    }

    
    const handleDeparture = (e) => {
        putPickUpTime({ id: pickUpTime.ID, status: e.target.value, start_time : pickUpTime.star_time }).then((r) => {
            console.log(r)
            setCheckStatus(!checkStatus)
        }).catch(err => {
            console.log(err)
            enqueueSnackbar("error lors de la confirmation du départ", {
                variant: 'error',
            });
        });
        handleClose()
    };

    return (
        <>
            <Stack
                textAlign="center"
                alignContent="center"
                alignItems="center"
                spacing={2}
            >
                <Stack>
                    <Typography variant='h4'>
                        Il est actuellement
                    </Typography>
                    <Typography variant='h1'>
                        {formattedTime}
                    </Typography>
                    <Typography variant='h4'>
                        le {formattedDate}.
                    </Typography>
                </Stack>
                <Typography p={1}>
                    Pointer votre
                </Typography>
                {pickUpTime && pickUpTime.status === 'arrived validated' ?
                    <CircleButton variant="contained" color='error' onClick={handleClickOpen}>
                        <Typography variant='body'>
                            Départ
                        </Typography>
                    </CircleButton> :
                    <CircleButton variant="contained" color='success' onClick={() => handleCreatePickUpTime()} value="arrived validated">
                        <Typography variant='body'>
                            Arrivée
                        </Typography>
                    </CircleButton>
                }
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogContent>
                        <DialogTitle id="alert-dialog-title">
                            Confirmez votre départ
                        </DialogTitle>
                    </DialogContent>
                    <Stack direction="row" p={1} alignContent="center" justifyContent="center"> 

                        <Button onClick={handleClose}>Non</Button>
                        <Button style={{ backgroundColor: "red", color: "white" }} onClick={(e) => handleDeparture(e)} value="departure validate" autoFocus>
                            Oui
                        </Button>
                    </Stack>
                </Dialog>
                <Typography p={1} variant='caption'>
                    Vous allez pointer votre arrivée. en cas d'erreur contactez votre responsable. *
                </Typography>
            </Stack >
        </>
    );
}

const mapStateToProps = state => {
    return {
        id: state.id,
        username: state.username,
    };
};

export default connect(mapStateToProps)(TimeClock);