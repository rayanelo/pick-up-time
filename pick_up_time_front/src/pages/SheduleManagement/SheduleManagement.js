import { Helmet } from 'react-helmet-async';
import { connect } from 'react-redux';
// @mui
import { Container } from '@mui/material';
// components
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';

import { useSettingsContext } from '../../components/settings';
import ManagerShedule from './components/ManagerShedule';
import UserShedule from './components/UserShedule';

// ----------------------------------------------------------------------

export function SheduleManagement(props) {
  const { themeStretch } = useSettingsContext();

  return (
    <>
      <Helmet>
        <title> Page Three | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'xl'} >
        <CustomBreadcrumbs
          heading="Gestion des horaires" />
        {
          props.role === "admin" ?
            <ManagerShedule />
            :
            <UserShedule />
        }
      </Container>
    </>
  );
}


const mapStateToProps = state => {
  return {
    id: state.id,
    username: state.username,
    role: state.role,
  };
};

export default connect(mapStateToProps)(SheduleManagement);