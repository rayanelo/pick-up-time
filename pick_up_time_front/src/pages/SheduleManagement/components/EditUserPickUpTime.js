import * as React from 'react';
import { useState } from 'react';
import dayjs from 'dayjs';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { putPickUpTime } from '../../../services';
import { useSnackbar } from '../../../components/snackbar';

// @mui
import {
    Button,
    Stack,
    DialogActions,
    Dialog,
    DialogContent,
    DialogTitle,
    Typography,
} from '@mui/material';
import Iconify from "../../../components/iconify"
import { IconButton } from '@mui/material';

export default function EditUserPickUpTime({ data , setLoading }) {
    const EditedStartPickUpTIme = new Date(data.star_time)
    const EditedEndPickUpTIme = new Date(data.end_time)
    let NewStartPickUpTime = new Date(data.star_time)
    let NewEndPickUpTime = new Date(data.end_time)
    const { enqueueSnackbar } = useSnackbar();
    const value = dayjs('2022-04-17T15:30')
    const [open, setOpen] = useState(false);

    const SetNewStartPickUpTime = (newTimer) => {
         NewStartPickUpTime = new Date(EditedStartPickUpTIme.getFullYear(), EditedStartPickUpTIme.getMonth(), EditedStartPickUpTIme.getDate(), newTimer.$H, newTimer.$m, 0, 0);
    }

    const SetNewEndPickUpTime = (newTimer) => {
         NewEndPickUpTime = new Date(EditedEndPickUpTIme.getFullYear(), EditedEndPickUpTIme.getMonth(), EditedEndPickUpTIme.getDate(), newTimer.$H, newTimer.$m, 0, 0);
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const HandlePickUpTime = () => {
        putPickUpTime({ id: data.ID, status: "edited by admin" , star_time : NewStartPickUpTime , end_time : NewEndPickUpTime }).then((r) => {
            console.log(r)
            // setCheckStatus(!checkStatus)
        }).catch(err => {
            console.log(err)
            enqueueSnackbar("error lors de la confirmation du départ", {
                variant: 'error',
            });
        });
        setLoading(true)
        handleClose()
    };

    return (
        <div>
            <IconButton aria-label="edit" onClick={handleClickOpen} >
                <Iconify icon="material-symbols:edit-calendar-outline" width={23} sx={{ color: "#00EA67" }} />
            </IconButton>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Modifier l'horaire de l'utilisateur"}
                </DialogTitle>
                <DialogContent>
                    <Stack p={1} alignContent="center" alignItems="center">
                        <Typography variant="body2" color="text.secondary">
                            chagement d'horaire du
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {EditedStartPickUpTIme.getDate()}/{EditedStartPickUpTIme.getMonth()}/{EditedStartPickUpTIme.getFullYear()}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            de l'utilsiateur {data.username}
                        </Typography>
                    </Stack>

                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DemoContainer components={['TimePicker', 'TimePicker']}>
                            <TimePicker
                                ampm={false}
                                label="Heure d'arrivé"
                                value={value}
                                onChange={(newValue) => SetNewStartPickUpTime(newValue)}
                            />
                            <TimePicker
                                ampm={false}
                                label="Heure de départ"
                                value={value}
                                onChange={(newValue) => SetNewEndPickUpTime(newValue)}
                            />
                        </DemoContainer>
                    </LocalizationProvider>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Annuler</Button>
                    <Button onClick={() => HandlePickUpTime()} autoFocus sx={{ }}>
                        Valider
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}