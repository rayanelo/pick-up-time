import React from 'react'
import { useState, useEffect } from 'react';
import formatDate from '../../../utils/formatDate';
// @mui
import {
    Table,
    TableBody,
    TableRow,
    TableHead,
    TableCell,
    Paper,
    TableContainer,
} from '@mui/material';
import Iconify from "../../../components/iconify"
import { getPickUpTimeByUser } from '../../../services';
import Label from '../../../components/label';

export default function UserShedule() {
    const [pickUpTime, setPickUpTime] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        Promise.all([
            getPickUpTimeByUser(),
        ]).then(([
            responseGetPickUpTimeByUser,
        ]) => {
            setPickUpTime(responseGetPickUpTimeByUser.pick_up_time)
            setLoading(false)
        }).catch(err => {
            console.log(err);
        });
    }, [loading])

    return (
        <> {
            !loading ?
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Utilisateur </TableCell>
                                <TableCell align="center">Date</TableCell>
                                <TableCell align="center">Heure d'arrivé</TableCell>
                                <TableCell align="center">Heure de départ</TableCell>
                                <TableCell align="center">Status</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {pickUpTime.map((row) => (
                                <TableRow
                                    key={row.ID}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {row.username}
                                    </TableCell>
                                    <TableCell align="center">{formatDate(row.star_time, "MM-DD")}</TableCell>
                                    <TableCell align="center">{formatDate(row.star_time, "HH:mm:ss")}</TableCell>
                                    <TableCell align="center">
                                        {row.status === "arrived validated" ?
                                            <Iconify icon="ic:outline-timer" width={23} sx={{ color: "#00EA67" }} />
                                            :
                                            formatDate(row.end_time, "HH:mm:ss")
                                        }
                                    </TableCell>
                                    <TableCell align="center">
                                        {
                                            <Label
                                                variant="soft"
                                                color={
                                                    (row.status === 'departure validate' && 'success') ||
                                                    (row.status === 'arrived validated' && 'warning') ||
                                                    (row.status === 'edited by admin' && 'warning') ||
                                                    (row.status === 'have to change' && 'error')
                                                }
                                                sx={{ textTransform: 'capitalize' }}
                                            >
                                                {(row.status === 'departure validate' && 'Valider') ||
                                                    (row.status === 'arrived validated' && 'En cours') ||
                                                    (row.status === 'edited by admin' && 'Modifié par votre résponsable') ||
                                                    (row.status === 'have to change' && 'À confirmer')
                                                }
                                            </Label>
                                        }
                                    </TableCell>
                                    <TableCell align="center">
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer >
                : <div> chargement ...</div>
        }
        </>

    )
}

