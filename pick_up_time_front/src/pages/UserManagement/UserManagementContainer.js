import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
// @mui
import { Container } from '@mui/material';
// components
import { useSettingsContext } from '../../components/settings';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import Spinner from '../../components/spinner';
import AddUser from './components/AddUser';
import UserTabManagement from './components/UserTabManagement';
import { getAllUser } from '../../services';
// ----------------------------------------------------------------------

export default function UserManagementContainer() {
  const { themeStretch } = useSettingsContext();
  const [isLoading, setIsLoarding] = useState(true)
  const [allUsers , setAllUsers] = useState()

  useEffect(() => {
    Promise.all([
      getAllUser()
    ]).then(([
      responseGetAllUser,
    ]) => {
      setAllUsers(responseGetAllUser.users)
      setIsLoarding(false)
    }).catch(err => {
      console.log(err);
    });
  }, [isLoading])

  return (
    <>
      <Helmet>
        <title> Page Two | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'xl'}>
        <CustomBreadcrumbs
          heading="Gestion des utilisateurs"
          action={
            <AddUser setIsLoarding={setIsLoarding} />
          }
        />
        {
          isLoading ? <Spinner /> :
            <UserTabManagement TabData={allUsers}  setIsLoarding={setIsLoarding} />
        }
      </Container>
    </>
  );
}
