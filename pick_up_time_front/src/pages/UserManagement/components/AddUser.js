import * as React from 'react';
import * as Yup from 'yup';
// form
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// Mui
import {
    Button,
    DialogTitle,
    Dialog,
    DialogActions,
    DialogContent,
    Stack,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
// components
import FormProvider, {
    RHFTextField,
} from '../../../components/hook-form';
import Iconify from '../../../components/iconify/Iconify';
import { useSnackbar } from '../../../components/snackbar';

// services
import { createUser } from '../../../services';

export default function AddUser({ setIsLoarding }) {
    const { enqueueSnackbar } = useSnackbar();
    const [open, setOpen] = React.useState(false);

    const Schema = Yup.object().shape({
        firstname: Yup.string().matches(/^[a-zA-Z]+$/gi, "Votre saisie est incorrecte merci de vérifier "),
        lastname: Yup.string().matches(/^[a-zA-Z]+$/gi, "Votre saisie est incorrecte merci de vérifier "),
        email: Yup.string().email().typeError('email incorrect'),
        password: Yup.string().required('le mot de passe et obligatoire')
            .matches(
                /^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$/,
                "Votre mot de pass doit contenir 8 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial"
            ),
        confirmPwd: Yup.string()
            .required('confiramtion du mot de passe obligatoire')
            .oneOf([Yup.ref('password'), null], 'les mots de passse ne sont pas identiques'),
    });

    const methods = useForm({
        resolver: yupResolver(Schema),
    });

    const {
        // reset,
        // setValue,
        handleSubmit,
        formState: { isSubmitting },
    } = methods;

    const onSubmit = (data) => {
        createUser(data).then((r) => {
            setIsLoarding(true)
            enqueueSnackbar('un nouvelle utilisateur à été ajouté', {
                variant: 'success',
            });
        }).catch(err => {
            enqueueSnackbar("Erreur lors de l'ajout", {
                variant: 'error',
            });
        });
        handleClose();
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button variant="contained" onClick={handleClickOpen} startIcon={<Iconify icon="eva:plus-fill" />}>
                Ajouter
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-add-user"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Renseigner les informations"}
                </DialogTitle>
                    <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
                        <DialogContent>
                            <Stack spacing={2} p={1}>
                                <Stack direction="row" spacing={2} p={1}>
                                    <RHFTextField size="small" name="lastname" label="Nom" />
                                    <RHFTextField size="small" name="firstname" label="Prénom" />
                                </Stack>
                                <Stack direction="row" spacing={2} p={1}>
                                    <RHFTextField size="small" name="email" label="Email" />
                                    <RHFTextField size="small" name="phone" label="Télephone" />
                                </Stack>
                                <RHFTextField size="small" name="username" label="Username" />
                                <RHFTextField size="small" name="password" type="password" label="mot de passe" />
                                <RHFTextField size="small" name="confirmPwd" type="password" label="confirmez le mot de passe" />
                            </Stack>
                        </DialogContent>

                        <DialogActions>
                            <Button onClick={handleClose} sx={{ color: "red" }}>Annuler</Button>
                            <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                                Ajouter
                            </LoadingButton>
                        </DialogActions>
                    </FormProvider>
            </Dialog>
        </div>
    );
}