import React, { } from 'react'
import {
    Table,
    IconButton,
    Paper,
    TableRow,
    TableHead,
    TableBody,
    TableCell,
    TableContainer,
    DialogTitle,
    DialogContentText,
    DialogActions,
    DialogContent,
    Dialog,
    Button,
} from '@mui/material';
// component
import DeleteIcon from '@mui/icons-material/Delete';
import { useSnackbar } from '../../../components/snackbar';
import { deleteUser } from '../../../services';

export default function UserTabManagement({ TabData, setIsLoarding }) {
    const { enqueueSnackbar } = useSnackbar();
    const [open, setOpen] = React.useState(false);

    const handleDeleteUser = (id) => {
        deleteUser(id).then(() => {
            enqueueSnackbar('utilisateur suprimée', {
                variant: 'success',
            });
            setIsLoarding(true)
        }).catch((err) => {
            enqueueSnackbar("error lors de la supression de l'utilsateur", {
                variant: 'error',
            });
            console.log(err);
        })
        handleClose()
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="left">Username</TableCell>
                        <TableCell align="left">Nom</TableCell>
                        <TableCell align="left">Prénom</TableCell>
                        <TableCell align="left">Email</TableCell>
                        <TableCell align="left">Télephone</TableCell>
                        <TableCell align="left"></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {TabData?.map((row, index) => (
                        <TableRow
                            key={index}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell align="left">{row.username}</TableCell>
                            <TableCell align="left">{row.lastname}</TableCell>
                            <TableCell align="left">{row.firstname}</TableCell>
                            <TableCell align="left">{row.email}</TableCell>
                            <TableCell align="left">{row.phone}</TableCell>
                            <TableCell align="center">
                                <IconButton aria-label="delete" onClick={handleClickOpen}>
                                    <DeleteIcon color='error' />
                                </IconButton>
                                <Dialog
                                    open={open}
                                    onClose={handleClose}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogContent>
                                        <DialogTitle id="alert-dialog-title">
                                            Supression de l'utilisateur
                                        </DialogTitle>
                                        <DialogContentText id="alert-dialog-description">
                                            étes vous sur de vouloir suprimer l'utilisateur ?
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleClose}>Non</Button>
                                        <Button style={{ backgroundColor: "red", color: "white" }} onClick={() => handleDeleteUser(row.ID)} autoFocus>
                                            Oui
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}