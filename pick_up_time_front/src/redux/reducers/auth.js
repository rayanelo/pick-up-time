import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    isAuthenticated: false,
    id: null,
    email: null,
    telephone: null,
    username: null,
    firstname: null,
    lastname: null,
    role: null,
    error: null,
    loading: true,
};

const authStart = (state, action) => {
    return updateObject(state, { error: null, loading: true });
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        isAuthenticated: true,
        id: action.id,
        email: action.email,
        telephone: action.telephone,
        username: action.username,
        firstname: action.firstname,
        lastname: action.lastname,
        role : action.role,
        error: null,
        loading: false
    });
}

const authFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false,
        isAuthenticated: false,
    });
}

const authLogout = (state, action) => {
    return updateObject(state, { ...initialState, loading: false })
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_LOGOUT: return authLogout(state, action);
        default:
            return state;
    }
}

export default reducer;