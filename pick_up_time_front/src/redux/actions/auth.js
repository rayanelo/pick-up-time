import axios from 'axios';
import Cookies from 'js-cookie';
import jwt from 'jwt-decode';
import { authentication, autoAuthentication, logout } from "../../services"
import * as actionTypes from './actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (isAuthenticated, id, email, telephone, username, firstname, lastname, role) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        isAuthenticated: isAuthenticated,
        id: id,
        email: email,
        telephone: telephone,
        username: username,
        firstname: firstname,
        lastname: lastname,
        role: role,
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const authLogout = () => {
    axios.defaults.withCredentials = true
    logout()
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const auth = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            username: username,
            password: password,
        };
        return new Promise((resolve, reject) => {
            authentication(authData)
                .then((result) => {
                    const user = jwt(Cookies.get("jwt"));
                    dispatch(authSuccess(true, user.id, user.email, user.telephone, user.username, user.firstname, user.lastname, user.role));
                    resolve();
                })
                .catch((err) => {
                    dispatch(authFail(true));
                    reject();
                });
        });
    };
};

export const authCheckState = () => {
    return dispatch => {
        autoAuthentication().then((_) => {
            const user = jwt(Cookies.get("jwt"));
            dispatch(authSuccess(true, user.id, user.email, user.telephone, user.username, user.firstname, user.lastname, user.role));
        }).catch(err => {
            console.log(err);
            dispatch(authFail(null));
        });
    };
};
