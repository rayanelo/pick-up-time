package routes

import (
	"Server/internal/application/authentication"
	"Server/internal/application/pick_up_time"
	"Server/internal/application/user"
	"Server/internal/infrastructure/persistence"
	"Server/internal/interface/http"
	"log"

	"github.com/gin-gonic/gin"
)

// SetupRoutes : Setup all the routes
func SetupRoutes(router *gin.Engine, sqlServer *persistence.Repositories) {
	authMiddleware, err := authentication.MiddlewareFunc(sqlServer)
	if err != nil {
		log.Fatal("Erreur middleware JWT")
	}
	protected := router.Group("/api")
	{
		protected.Use(authMiddleware.MiddlewareFunc())

		//Authentication
		protected.GET("/login", authentication.Login)
		protected.GET("/logout", authMiddleware.LogoutHandler)

		//User
		protected.POST("/createUser", http.PostUser(user.PostUser(sqlServer)))
		protected.GET("/getAllUsers", http.GetAllUsers(user.GetAllUsers(sqlServer)))
		protected.DELETE("/deleteUser", http.DeleteUser(user.DeleteUser(sqlServer)))

		//PickUpTime
		protected.POST("/createPickUpTime", http.PostPickUpTime(pick_up_time.PostPickUpTime(sqlServer)))
		protected.GET("/getAllPickUpTime", http.GetAllPickUpTime(pick_up_time.GetAllPickUpTime(sqlServer)))
		protected.GET("/getLastPickUpTimeByUser", http.GetLastPickUpTimeByUser(pick_up_time.GetLastPickUpTimeByUser(sqlServer)))
		protected.GET("getPickUpTimeByUser", http.GetPickUpTimeByUser(pick_up_time.GetPickUpTimeByUser(sqlServer)))
		protected.PUT("/putPickUpTime", http.PutPickUpTime(pick_up_time.PutPickUpTime(sqlServer)))

	}
	router.POST("/api/signin", http.PostUser(user.PostUser(sqlServer)))
	router.POST("/api/login", authMiddleware.LoginHandler)
}
