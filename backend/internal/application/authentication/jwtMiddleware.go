package authentication

import (
	"context"
	"net/http"
	"os"
	"time"

	"Server/domain/entity"
	"Server/internal/infrastructure/persistence"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

var identityKey = "username"

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Login : Login auto auth response
func Login(c *gin.Context) {
	c.JSON(200, gin.H{
		"code": "200",
	})
}

func verifyUser(ctx context.Context, sqlServer *persistence.Repositories, credentials Credentials) (interface{}, error) {
	account, err := sqlServer.User.Get(entity.User{Username: credentials.Username})
	if err != nil {
		logrus.Error(err)
		return entity.User{}, jwt.ErrFailedAuthentication
	}
	if err = bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(credentials.Password)); err != nil {
		return entity.User{}, jwt.ErrFailedAuthentication
	}
	var user interface{}
	user, _ = sqlServer.User.Get(entity.User{Username: account.Username, ID: account.ID})
	return user, nil
}

// Middleware : jwt connection
func MiddlewareFunc(sqlServer *persistence.Repositories) (*jwt.GinJWTMiddleware, error) {
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "login zone",
		Key:         []byte(os.Getenv("JWT_SECRET")),
		Timeout:     time.Hour * 144,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if u, ok := data.(entity.User); ok {
				return jwt.MapClaims{
					"id":        u.ID,
					"username":  u.Username,
					"lastname":  u.Lastname,
					"firstname": u.Firstname,
					"phone":     u.Phone,
					"role":      u.Role,
					"email":     u.Email,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &entity.User{
				ID:       int(claims["id"].(float64)),
				Username: claims["username"].(string),
				Email:    claims["email"].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var credentials Credentials
			if err := c.ShouldBind(&credentials); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			return verifyUser(c.Request.Context(), sqlServer, credentials)
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			return true
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		LoginResponse: func(c *gin.Context, code int, token string, expire time.Time) {
			c.JSON(http.StatusOK, gin.H{
				"code": http.StatusOK,
			})
		},
		TokenLookup: "header: Authorization, query: token, cookie: jwt",

		SecureCookie: false,

		CookieHTTPOnly: false,

		SendCookie: true,

		TimeFunc: time.Now,
	})
	return authMiddleware, err
}
