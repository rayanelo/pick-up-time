package pick_up_time

import (
	"Server/domain/entity"
	"Server/internal/infrastructure/persistence"
	"context"
	"fmt"
	"time"
)

type PostPickUpTimeCmd func(ctx context.Context, pick_up_time entity.PickUpTime) error

func PostPickUpTime(sqlServer *persistence.Repositories) PostPickUpTimeCmd {
	return func(ctx context.Context, pick_up_time entity.PickUpTime) error {

		pick_up_time_check, err := sqlServer.PickUpTime.GetLastPickUpTimeByUser(pick_up_time.UserReference)
		if err != nil {
			fmt.Println(err)
		}
		// Check if already valide

		if pick_up_time_check.StartTime.Year() == time.Now().Year() &&
			pick_up_time_check.StartTime.Month() == time.Now().Month() &&
			pick_up_time_check.StartTime.Day() == time.Now().Day() &&
			pick_up_time_check.UserReference == pick_up_time.UserReference {
			return entity.ErrPickUpTimeAlreadyCreated
		} else {
			// Insert new Porduct

			err = sqlServer.PickUpTime.Insert(pick_up_time)
			if err != nil {
				fmt.Println(err)
				return entity.ErrPickUpTimeNotCreated
			}
			return nil
		}

	}
}
