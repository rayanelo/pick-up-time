package pick_up_time

import (
	"Server/domain/entity"
	"Server/internal/infrastructure/persistence"

	"github.com/gin-gonic/gin"
)

type GetAllPickUpTimeCmd func(c *gin.Context) ([]entity.PickUpTime, error)
type GetLastPickUpTimeByUserCmd func(c *gin.Context, userId int) (entity.PickUpTime, error)
type GetPickUpTimeByUserCmd func(c *gin.Context, userId int) ([]entity.PickUpTime, error)

func GetAllPickUpTime(sqlServer *persistence.Repositories) GetAllPickUpTimeCmd {
	return func(c *gin.Context) ([]entity.PickUpTime, error) {
		pick_up_time, _ := sqlServer.PickUpTime.GetAllPickUpTime()
		return pick_up_time, nil
	}
}

func GetLastPickUpTimeByUser(sqlClient *persistence.Repositories) GetLastPickUpTimeByUserCmd {
	return func(c *gin.Context, userId int) (entity.PickUpTime, error) {
		pick_up_time, _ := sqlClient.PickUpTime.GetLastPickUpTimeByUser(userId)
		return pick_up_time, nil
	}
}

func GetPickUpTimeByUser(sqlClient *persistence.Repositories) GetPickUpTimeByUserCmd {
	return func(c *gin.Context, userId int) ([]entity.PickUpTime, error) {
		pick_up_time, _ := sqlClient.PickUpTime.GetPickUpTimeByUser(userId)
		return pick_up_time, nil
	}
}
