package pick_up_time

import (
	"Server/domain/entity"
	"Server/internal/infrastructure/persistence"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

type PutPickUpTimeCmd func(c *gin.Context, product entity.PickUpTime) error

func PutPickUpTime(sqlClient *persistence.Repositories) PutPickUpTimeCmd {
	return func(c *gin.Context, pick_up_time entity.PickUpTime) error {

		fmt.Println(pick_up_time.Status)

		if pick_up_time.Status == "edited by admin" {
			err := sqlClient.PickUpTime.UpdatesPickUpTime(pick_up_time, map[string]interface{}{
				"start_time": pick_up_time.StartTime,
				"end_time":   pick_up_time.EndTime,
				"status":     pick_up_time.Status,
			})
			if err != nil {
				fmt.Println(err)
				return entity.ErrPickUpTimeNotFound
			}
		} else {
			err := sqlClient.PickUpTime.UpdatesPickUpTime(pick_up_time, map[string]interface{}{
				"end_time": time.Now().Local(),
				"status":   pick_up_time.Status,
			})
			if err != nil {
				fmt.Println(err)
				return entity.ErrPickUpTimeNotFound
			}
		}
		return nil
	}
}
