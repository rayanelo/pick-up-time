package user

import (
	"Server/domain/entity"
	"Server/internal/infrastructure/persistence"

	"github.com/gin-gonic/gin"
)

type DeleteUserCmd func(c *gin.Context, ID int) error

func DeleteUser(sqlServer *persistence.Repositories) DeleteUserCmd {
	return func(c *gin.Context, ID int) error {
		raw_material, err := sqlServer.User.Get(entity.User{ID: ID})
		if err != nil {
			return entity.ErrUserNotFound
		}

		//Delete comment
		err = sqlServer.User.Delete(raw_material)
		if err != nil {
			return entity.ErrUserDeleted
		}
		return nil
	}
}
