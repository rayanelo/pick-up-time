package user

import (
	"Server/domain/entity"
	"Server/internal/infrastructure/persistence"

	"github.com/gin-gonic/gin"
)

type GetAllUserCmd func(c *gin.Context) ([]entity.User, error)

func GetAllUsers(sqlServer *persistence.Repositories) GetAllUserCmd {
	return func(c *gin.Context) ([]entity.User, error) {
		users, _ := sqlServer.User.GetAllUser()
		return users, nil
	}
}
