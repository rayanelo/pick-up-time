package user

import (
	"Server/domain/entity"
	"Server/internal/infrastructure/persistence"
	"context"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type PostUserCmd func(ctx context.Context, user entity.User) error

func PostUser(sqlServer *persistence.Repositories) PostUserCmd {
	return func(ctx context.Context, user entity.User) error {

		// Check Username validity
		_, err := sqlServer.User.Get(entity.User{Username: user.Username})
		if err == nil {
			logrus.Errorf("error user already exist: %v", err)
			return entity.ErrUserAlreadyExist
		}

		_, err2 := sqlServer.User.Get(entity.User{Email: user.Email})
		if err2 == nil {
			logrus.Errorf("error Email already exist: %v", err)
			return entity.ErrEmailAlreadyExist
		}

		// Hash password
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 9)
		if err != nil {
			logrus.Error(entity.ErrPasswordHash)
			return entity.ErrPasswordHash
		}
		user.Password = string(hashedPassword)

		// Insert new user
		err = sqlServer.User.Insert(user)
		if err != nil {
			logrus.Error(err)
			return entity.ErrUserNotCreated
		}
		return nil
	}
}
