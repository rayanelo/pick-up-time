package http

import (
	"Server/domain/entity"
	"Server/internal/application/user"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func PostUser(cmd user.PostUserCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		var req entity.User
		if err := c.ShouldBind(&req); err != nil {
			c.Status(http.StatusBadRequest)
			return
		}
		err := cmd(c.Request.Context(), req)
		if err != nil {
			switch err {
			case entity.ErrUserAlreadyExist:
				c.JSON(http.StatusConflict, gin.H{
					"error": err.Error(),
				})
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
			}
		}
		c.Status(http.StatusOK)
	}
}

func GetAllUsers(cmd user.GetAllUserCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		users, err := cmd(c)
		if err != nil {
			switch err {
			case entity.ErrUserNotFound:
				c.JSON(http.StatusNotFound, gin.H{
					"error": err,
				})
				return
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": err,
				})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{
			"users": users,
		})
	}
}

func DeleteUser(cmd user.DeleteUserCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Query("id"))
		if err != nil {
			c.Status(http.StatusBadRequest)
		}
		if id <= 0 {
			c.Status(http.StatusBadRequest)
		}
		err = cmd(c, id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, gin.H{})
	}
}
