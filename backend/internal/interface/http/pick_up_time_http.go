package http

import (
	"Server/domain/entity"
	"Server/internal/application/pick_up_time"
	"fmt"
	"net/http"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

func PostPickUpTime(cmd pick_up_time.PostPickUpTimeCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		var req entity.PickUpTime
		if err := c.ShouldBind(&req); err != nil {
			c.Status(http.StatusBadRequest)
			return
		}
		err := cmd(c.Request.Context(), req)
		if err != nil {
			switch err {
			case entity.ErrPickUpTimeNotCreated:
				c.JSON(http.StatusConflict, gin.H{
					"error": err.Error(),
				})
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
			}
		}
		c.Status(http.StatusOK)
	}
}

func GetAllPickUpTime(cmd pick_up_time.GetAllPickUpTimeCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		pick_up_time, err := cmd(c)
		if err != nil {
			switch err {
			case entity.ErrPickUpTimeNotFound:
				c.JSON(http.StatusNotFound, gin.H{
					"error": err,
				})
				return
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": err,
				})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{
			"pick_up_time": pick_up_time,
		})
	}
}

func GetLastPickUpTimeByUser(cmd pick_up_time.GetLastPickUpTimeByUserCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		userId := int(claims["id"].(float64))
		pick_up_time, err := cmd(c, userId)
		if err != nil {
			switch err {
			case entity.ErrPickUpTimeNotFound:
				c.JSON(http.StatusNotFound, gin.H{
					"error": err,
				})
				return
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": err,
				})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{
			"pick_up_time": pick_up_time,
		})
	}
}

func GetPickUpTimeByUser(cmd pick_up_time.GetPickUpTimeByUserCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		userId := int(claims["id"].(float64))
		pick_up_time, err := cmd(c, userId)
		if err != nil {
			switch err {
			case entity.ErrPickUpTimeNotFound:
				c.JSON(http.StatusNotFound, gin.H{
					"error": err,
				})
				return
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": err,
				})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{
			"pick_up_time": pick_up_time,
		})
	}
}

func PutPickUpTime(cmd pick_up_time.PutPickUpTimeCmd) gin.HandlerFunc {
	return func(c *gin.Context) {
		var req entity.PickUpTime
		fmt.Println(req.Status)
		if err := c.ShouldBind(&req); err != nil {
			c.Status(http.StatusBadRequest)
			return
		}
		err := cmd(c, req)
		if err != nil {
			switch err {
			case entity.ErrPickUpTimeNotFound:
				c.JSON(http.StatusNotFound, gin.H{
					"error": err,
				})
				return
			default:
				c.JSON(http.StatusInternalServerError, gin.H{
					"error": err,
				})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{})
	}
}
