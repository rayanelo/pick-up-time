package persistence

import (
	"Server/domain/entity"
	"Server/domain/repository"
	"errors"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type UserRepo struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepo {
	err := db.AutoMigrate(&entity.User{})
	if err != nil {
		logrus.Errorf("error initializing User: %v", err)
	}
	return &UserRepo{db}
}

func (c *UserRepo) Insert(user entity.User) error {
	if err := c.db.Create(&user).Error; err != nil {
		return err
	}
	return nil
}

func (c *UserRepo) Get(user entity.User) (entity.User, error) {
	if err := c.db.Where(&user).First(&user).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return entity.User{}, entity.ErrUserNotFound
		}
		return entity.User{}, err
	}
	return user, nil
}

func (f *UserRepo) Delete(rawMaterial entity.User) error {
	if err := f.db.Delete(&rawMaterial).Error; err != nil {
		return err
	}
	return nil
}

func (c *UserRepo) GetAllUser() ([]entity.User, error) {
	var User []entity.User
	if err := c.db.Table("users").Order("created_at asc").Find(&User).Error; err != nil {
		return User, entity.ErrUserNotFound
	}
	return User, nil
}

var _ repository.UserRepository = &UserRepo{}
