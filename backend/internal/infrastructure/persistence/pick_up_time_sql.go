package persistence

import (
	"Server/domain/entity"
	"Server/domain/repository"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type PickUpTimeRepo struct {
	db *gorm.DB
}

func NewPickUpTimeRepository(db *gorm.DB) *PickUpTimeRepo {
	err := db.AutoMigrate(&entity.PickUpTime{})
	if err != nil {
		logrus.Errorf("error initializing Product: %v", err)
	}
	return &PickUpTimeRepo{db}
}

func (c *PickUpTimeRepo) Insert(pickUpTime entity.PickUpTime) error {
	pickUpTime.StartTime = time.Now().Local()
	fmt.Println("POST", pickUpTime.StartTime)
	if err := c.db.Create(&pickUpTime).Error; err != nil {
		return err
	}
	return nil
}

func (c *PickUpTimeRepo) GetAllPickUpTime() ([]entity.PickUpTime, error) {
	var pickUpTime []entity.PickUpTime
	if err := c.db.Table("pick_up_times").Order("created_at DESC").Find(&pickUpTime).Error; err != nil {
		return pickUpTime, entity.ErrPickUpTimeNotFound
	}
	return pickUpTime, nil
}

func (c *PickUpTimeRepo) GetPickUpTimeByUser(userId int) ([]entity.PickUpTime, error) {
	var pickUpTime []entity.PickUpTime
	if err := c.db.Table("pick_up_times").Where("user_reference = ?", userId).Order("created_at DESC").Find(&pickUpTime).Error; err != nil {
		return pickUpTime, entity.ErrPickUpTimeNotFound
	}
	return pickUpTime, nil
}

func (c *PickUpTimeRepo) GetLastPickUpTimeByUser(userId int) (entity.PickUpTime, error) {
	var lastPickUpTime entity.PickUpTime
	if err := c.db.Table("pick_up_times").Last(&lastPickUpTime).Where("user_reference = ?", userId).Error; err != nil {
		return lastPickUpTime, entity.ErrPickUpTimeNotFound
	}
	return lastPickUpTime, nil
}

func (c *PickUpTimeRepo) UpdatesPickUpTime(pick_up_times entity.PickUpTime, update map[string]interface{}) error {
	if err := c.db.Model(&entity.PickUpTime{}).Where("id = ?", pick_up_times.ID).Updates(update).Error; err != nil {
		return err
	}
	return nil
}

var _ repository.PickUpTimeRepository = &PickUpTimeRepo{}
