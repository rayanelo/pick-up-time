package persistence

import (
	"Server/domain/repository"
	"fmt"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	// Used for the postgres driver
)

type Repositories struct {
	User       repository.UserRepository
	PickUpTime repository.PickUpTimeRepository
}

// NewPostgresql :  Connect to Databases and create repositories
func NewPostgresql() (*Repositories, error) {
	dbURI := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable TimeZone=Europe/Paris",
		os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_NAME"))
	dbServer, err := gorm.Open(postgres.Open(dbURI), &gorm.Config{
		Logger:      logger.Default.LogMode(logger.Silent),
		PrepareStmt: true,
	})
	if err != nil {
		return &Repositories{}, err
	}

	return &Repositories{
		User:       NewUserRepository(dbServer),
		PickUpTime: NewPickUpTimeRepository(dbServer),
	}, nil
}
