package repository

import "Server/domain/entity"

type UserRepository interface {
	Insert(users entity.User) error
	Get(user entity.User) (entity.User, error)
	GetAllUser() ([]entity.User, error)
	Delete(rawMaterial entity.User) error
}
