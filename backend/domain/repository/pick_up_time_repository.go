package repository

import "Server/domain/entity"

type PickUpTimeRepository interface {
	Insert(pickUpTime entity.PickUpTime) error
	GetAllPickUpTime() ([]entity.PickUpTime, error)
	GetLastPickUpTimeByUser(userId int) (entity.PickUpTime, error)
	GetPickUpTimeByUser(userId int) ([]entity.PickUpTime, error)
	UpdatesPickUpTime(pick_up_times entity.PickUpTime, update map[string]interface{}) error
}
