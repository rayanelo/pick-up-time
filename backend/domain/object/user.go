package object

type Password struct {
	OldPassword string `json:"oldpassword"`
	NewPassword string `json:"newpassword"`
}
