package entity

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

var ErrPickUpTimeNotCreated = errors.New("pick up time has not been created")
var ErrPickUpTimeNotModified = errors.New("pick up time has not been modified")
var ErrPickUpTimeNotFound = errors.New("pick up time has not been found")
var ErrPickUpTimeAlreadyExist = errors.New("pick up time already exist")
var ErrPickUpTimeDeleted = errors.New("can't delete pick up time")
var ErrPickUpTimeAlreadyCreated = errors.New("pick up time already created at this date")

type PickUpTime struct {
	ID            int       `gorm:"primary_key;autoIncrement"`
	UserReference int       `json:"user_reference"`
	Username      string    `json:"username"`
	Status        string    `json:"status"`
	StartTime     time.Time `json:"star_time"`
	EndTime       time.Time `json:"end_time"`

	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt
}
