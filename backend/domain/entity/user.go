package entity

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

var ErrUserNotCreated = errors.New("user has not been created")
var ErrUserNotModified = errors.New("user has not been modified")
var ErrUserNotFound = errors.New("user has not been found")
var ErrUserAlreadyExist = errors.New("user already exist")
var ErrUserDeleted = errors.New("can't delete user")
var ErrEmailAlreadyExist = errors.New("email already exist")
var ErrPasswordHash = errors.New("password has not been hashed")
var ErrPasswordCompare = errors.New("password does not match")

type User struct {
	ID        int    `gorm:"primary_key;autoIncrement"`
	Username  string `json:"username"`
	Email     string `json:"email"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Phone     string `json:"phone"`
	Role      string `json:"role"`
	Password  string `json:"password"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt
}
