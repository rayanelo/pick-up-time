package main

import (
	"Server/internal/infrastructure/persistence"
	"Server/routes"
	"fmt"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

func init() {
	//Load environmental variables.
	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("unable to read env file")
	}
}

func main() {
	sqlServer, err := persistence.NewPostgresql()
	if err != nil {
		logrus.Fatalf("error initializing configuration: %v", err)
	}

	// cors policy
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowOrigins = []string{"http://localhost:3000"}
	corsConfig.AllowCredentials = true

	//Gin gonic router
	if os.Getenv("ENV") == "prod" {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.Default()
	router.Use(cors.New(corsConfig))

	routes.SetupRoutes(router, sqlServer)

	err = router.Run(":8080")
	if err != nil {
		fmt.Println(err)
		logrus.Fatal("error while running the router")
	}
}
